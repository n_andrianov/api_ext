CREATE SEQUENCE dcts.users_ext_id_seq;
CREATE TABLE dcts.users_ext
(
    id integer NOT NULL DEFAULT nextval('dcts.users_ext_id_seq'::regclass),
    login character varying(64) NOT NULL,
    password character varying(32) NOT NULL,
    mail character varying(256),
    CONSTRAINT users_ext_pkey PRIMARY KEY (id),
    CONSTRAINT users_ext_login_key UNIQUE (login),
    CONSTRAINT users_ext_mail_key UNIQUE (mail)
)
CREATE TABLE dcts.users_ext_balance
(
	login text NOT NULL,
  balance text NOT NULL,
  CONSTRAINT users_ext_balance_pkey PRIMARY KEY (login)
)
CREATE TABLE dcts.wallets_ext
(
	login text NOT NULL,
  wallet text NOT NULL
)