package com.idt.rtm.web.ext.model;

public class TokenClaims {

    public static final String LOGIN = "login";

    public static final String ID = "id";

    public static final String GRANTS = "grants";

    public static final String CREATION_DATE = "creationDate";

    public static final String SUPER_USER = "superUser";
}
