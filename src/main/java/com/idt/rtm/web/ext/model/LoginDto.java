package com.idt.rtm.web.ext.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class LoginDto {

    private String token;

    private String login;

    private String email;
}