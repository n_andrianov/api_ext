package com.idt.rtm.web.ext.web;

import com.idt.rtm.web.ext.model.AuthDataDto;
import com.idt.rtm.web.ext.model.LoginDto;
import com.idt.rtm.web.ext.service.AuthorizationService;
import com.idt.rtm.web.ext.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class AuthController {

    private final AuthorizationService authorizationService;

    public AuthController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginDto> register(@RequestBody @Validated AuthDataDto authDataDto) {
        authorizationService.register(authDataDto);
        return ResponseEntity.ok(authorizationService.login(authDataDto));
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginDto> getToken(@RequestBody @Validated AuthDataDto authDataDto) {
        log.trace("Got auth request from user: {}, pass is present: {}", authDataDto.getLogin(), authDataDto.getPassword() != null);
        return ResponseEntity.ok(authorizationService.login(authDataDto));
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(@RequestHeader(value = "Authorization") String headerValue) {
        String token = Utils.extractTokenFromHeader(headerValue);
        authorizationService.logout(token);
    }
}
