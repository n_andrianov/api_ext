package com.idt.rtm.web.ext.service.impl;

import com.idt.rtm.web.ext.mapper.PostgresMapper;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.service.WalletService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class WalletServiceImpl implements WalletService {

    private final PostgresMapper postgresMapper;

    private final String schema;

    public WalletServiceImpl(PostgresMapper postgresMapper,
                           @Value("${app.schema_name}") String schema){
        this.postgresMapper = postgresMapper;
        this.schema = schema;
    }

    @Override
    public void addWallet(User user) {
        postgresMapper.addWallet(user, schema);
    }
}
