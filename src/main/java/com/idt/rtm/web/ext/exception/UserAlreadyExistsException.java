package com.idt.rtm.web.ext.exception;

public class UserAlreadyExistsException extends RuntimeException {

    public UserAlreadyExistsException(String login, String email) {
        super(String.format("User with such login [%s] or email [%s] already exist", login, email));
    }
}