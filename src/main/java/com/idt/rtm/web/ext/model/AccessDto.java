package com.idt.rtm.web.ext.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccessDto {

    private String module;

    private Set<String> grants = new HashSet<>();
}
