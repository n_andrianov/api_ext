package com.idt.rtm.web.ext.configuration;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.policy.WritePolicy;
import com.idt.rtm.web.ext.mapper.PostgresMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.Objects;

@Configuration
public class DataSourceConfiguration {

    @Value("${aerospike.hostname}")
    private String aerospikeHostname;

    @Value("${aerospike.port}")
    private int aerospikePort;

    @Bean(destroyMethod = "close")
    public AerospikeClient aerospikeSendClient() {
        WritePolicy writePolicy = new WritePolicy();
        writePolicy.sendKey = true;
        ClientPolicy policy = new ClientPolicy();
        policy.failIfNotConnected = true;
        policy.writePolicyDefault = writePolicy;
        return new AerospikeClient(policy, aerospikeHostname, aerospikePort);
    }

    @Bean
    public DataSource postgresDataSource(@Value("${postgresql.user}") String user,
                                         @Value("${postgresql.password}") String password,
                                         @Value("${postgresql.hostname}") String hostname,
                                         @Value("${postgresql.dbname}") String dbname,
                                         @Value("${postgresql.ssl}") String ssl,
                                         @Value("${postgresql.sslFactory}") String sslFactory) {
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName(org.postgresql.ds.PGSimpleDataSource.class.getName());
        config.setUsername(user);
        config.setPassword(password);
        config.addDataSourceProperty("databaseName", dbname);
        config.addDataSourceProperty("serverName", hostname);
        config.addDataSourceProperty("ssl", ssl);
        config.addDataSourceProperty("sslfactory", sslFactory);
        config.setPoolName("postgresDataSource");
        return new HikariDataSource(config);
    }

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource postgresDataSource) {
        return new DataSourceTransactionManager(postgresDataSource);
    }

    private org.apache.ibatis.session.Configuration myBatisConfiguration() {
        org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
        config.setCallSettersOnNulls(true);
        return config;
    }

    private SqlSessionFactoryBean sqlSessionFactory(DataSource tdApiPostgresDataSource) {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(tdApiPostgresDataSource);
        sqlSessionFactory.setConfiguration(myBatisConfiguration());
        return sqlSessionFactory;
    }

    @Bean
    public SqlSessionTemplate postgresSqlSessionTemplate(DataSource tdApiPostgresDataSource) throws Exception {
        return new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory(tdApiPostgresDataSource).getObject()));
    }

    @Bean
    public MapperFactoryBean<PostgresMapper> postgresMapper(SqlSessionTemplate postgresSqlSessionTemplate) {
        MapperFactoryBean<PostgresMapper> mapper = new MapperFactoryBean<>(PostgresMapper.class);
        mapper.setSqlSessionTemplate(postgresSqlSessionTemplate);
        return mapper;
    }
}