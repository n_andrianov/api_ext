package com.idt.rtm.web.ext.service.impl;

import com.idt.rtm.web.ext.exception.InvalidTokenParamException;
import com.idt.rtm.web.ext.exception.TokenExpiredException;
import com.idt.rtm.web.ext.model.TokenClaims;
import com.idt.rtm.web.ext.model.TokenStateEnum;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.model.UserTokenInfo;
import com.idt.rtm.web.ext.service.JwtService;
import com.idt.rtm.web.ext.service.UserTokenInfoStorageService;
import com.idt.rtm.web.ext.util.Utils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class JwtServiceImpl implements JwtService {

    private final JwtParser parser;
    private final UserTokenInfoStorageService tokenInfoStorage;
    private final Key key;
    private final Long tokenLifetime;

    public JwtServiceImpl(
            UserTokenInfoStorageService tokenInfoStorage,
            @Value("${app.token.secret-key}") String secret,
            @Value("${app.token.lifetime}") Long tokenLifetime) {
        this.tokenInfoStorage = tokenInfoStorage;
        key = Keys.hmacShaKeyFor(secret.getBytes());
        parser = Jwts.parser().setSigningKey(key);
        this.tokenLifetime = tokenLifetime;
    }

    @Override
    public String generate(User user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(TokenClaims.LOGIN, user.getLogin());
        claims.put(TokenClaims.ID, user.getId().toString());

        return Jwts.builder()
                .setSubject(user.getId().toString())
                .addClaims(claims)
                .setExpiration(Utils.getNewDate(tokenLifetime))
                .signWith(key)
                .compact();
    }

    @Override
    public void logout(String token) {
        Claims claims = getClaims(token);
        Long userId = Long.valueOf(claims.getSubject());
        banUserTokens(userId);
    }

    @Override
    public void isValid(String token){
        if (parser.isSigned(token)) {
            Jws<Claims> jws = parser.parseClaimsJws(token);
            Date expirationDate = jws.getBody().getExpiration();
            Date validUntil = Utils.getExpDate(tokenLifetime);
            if (expirationDate == null) {
                throw new InvalidTokenParamException("expirationDate");
            }
            if (expirationDate.before(validUntil)) {
                throw new TokenExpiredException(expirationDate);
            }
        }
    }

    @Override
    public Claims getClaims(String token) {
        try {
            return parser.parseClaimsJws(token).getBody();
        } catch (SignatureException e) {
            throw new InvalidTokenParamException(token);
        }
    }

    @Override
    public String refresh(Claims claims, Long userId){
        return Jwts.builder()
                .setSubject(userId.toString())
                .addClaims(claims)
                .setExpiration(Utils.getNewDate(tokenLifetime))
                .signWith(key)
                .compact();
    }

    private void banUserTokens(Long userId) {
        UserTokenInfo uti = new UserTokenInfo(
                TokenStateEnum.BAN,
                Utils.getCurrentDate(),
                Utils.getNewDate(tokenLifetime)
        );
        tokenInfoStorage.put(userId, uti);
    }
}
