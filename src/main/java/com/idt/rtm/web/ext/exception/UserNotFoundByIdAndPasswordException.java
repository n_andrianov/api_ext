package com.idt.rtm.web.ext.exception;

public class UserNotFoundByIdAndPasswordException extends RuntimeException {

    public UserNotFoundByIdAndPasswordException(String userId) {
        super(String.format("User with id [%s] and received password not found", userId));
    }
}
