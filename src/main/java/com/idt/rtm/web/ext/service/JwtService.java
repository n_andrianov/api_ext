package com.idt.rtm.web.ext.service;

import com.idt.rtm.web.ext.model.User;
import io.jsonwebtoken.Claims;

public interface JwtService {

    String generate(User user);

    void logout(String token);

    void isValid(String token);

    Claims getClaims(String token);

    String refresh(Claims claims, Long userId);
}
