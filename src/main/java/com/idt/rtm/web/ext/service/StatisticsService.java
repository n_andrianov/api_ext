package com.idt.rtm.web.ext.service;

import com.idt.rtm.web.ext.model.User;

public interface StatisticsService {
    String getStatistics(User user);
}
