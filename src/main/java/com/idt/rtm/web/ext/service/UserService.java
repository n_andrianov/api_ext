package com.idt.rtm.web.ext.service;

import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.model.UserInfo;

public interface UserService {
    UserInfo getUserInfo(User user);
}
