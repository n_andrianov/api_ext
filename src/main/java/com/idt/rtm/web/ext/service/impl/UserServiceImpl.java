package com.idt.rtm.web.ext.service.impl;

import com.idt.rtm.web.ext.mapper.PostgresMapper;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.model.UserInfo;
import com.idt.rtm.web.ext.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final PostgresMapper postgresMapper;

    private final String schema;

    public UserServiceImpl(PostgresMapper postgresMapper,
                           @Value("${app.schema_name}") String schema){
        this.postgresMapper = postgresMapper;
        this.schema = schema;
    }

    @Override
    public UserInfo getUserInfo(User user){
        UserInfo userInfo = postgresMapper.getUserInfo(user, schema);
        List<String> wallets = postgresMapper.getUserWallets(user, schema);
        userInfo.setWallets(wallets);
        return userInfo;
    }
}
