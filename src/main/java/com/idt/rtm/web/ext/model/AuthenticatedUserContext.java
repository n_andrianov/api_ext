package com.idt.rtm.web.ext.model;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

@Data
@Component
@RequestScope
public class AuthenticatedUserContext {

    private Long id;

    private String login;

    private boolean isSuper;

    private List<AccessDto> grants;
}
