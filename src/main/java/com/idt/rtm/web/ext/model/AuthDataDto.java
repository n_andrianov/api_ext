package com.idt.rtm.web.ext.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class AuthDataDto {

    @NotNull(message = "{login.empty}")
    @Size(min = 3, max = 64, message = "{login.length}")
    private String login;

    @NotNull(message = "{password.empty}")
    @Size(min = 6, max = 32, message = "{password.length}")
    private String password;

    private String mail;
}
