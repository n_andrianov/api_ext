package com.idt.rtm.web.ext.web;

import com.idt.rtm.web.ext.annotation.Authenticated;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.model.UserInfo;
import com.idt.rtm.web.ext.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UsersController {

    private final UserService userService;

    public UsersController(UserService userService){
        this.userService = userService;
    }

    @Authenticated
    @RequestMapping(value = "/user_info", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserInfo> getUserInfo(@RequestBody @Validated User user) {
        return ResponseEntity.ok(userService.getUserInfo(user));
    }
}
