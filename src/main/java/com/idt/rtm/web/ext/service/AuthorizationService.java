package com.idt.rtm.web.ext.service;

import com.idt.rtm.web.ext.model.AuthDataDto;
import com.idt.rtm.web.ext.model.LoginDto;

public interface AuthorizationService {

    LoginDto register(AuthDataDto authDataDto);

    LoginDto login(AuthDataDto authDataDto);

    void logout(String token);

    String checkAccess(String token);
}
