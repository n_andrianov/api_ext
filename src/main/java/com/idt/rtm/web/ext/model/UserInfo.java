package com.idt.rtm.web.ext.model;

import lombok.Data;

import java.util.List;

@Data
public class UserInfo {
    private String balance;
    private List<String> wallets;
}
