package com.idt.rtm.web.ext.service;

import com.idt.rtm.web.ext.model.UserTokenInfo;

/**
 * @author khorobrykh
 *
 * UserTokenInfo storage service which contains {@link com.idt.rtm.web.ext.model.UserTokenInfo} entities and provive access to them
 */
public interface UserTokenInfoStorageService {

    /**
     * Store info by user id
     *
     * @param userId - user's identifier
     * @param info   - info to store
     */
    void put(Long userId, UserTokenInfo info);

    /**
     * @param userId - user's identifier
     * @return {@link UserTokenInfo} related to user id or null
     */
    UserTokenInfo get(Long userId);

    /**
     * Removes info from storage, if it exist
     *
     * @param userId - user's identifier
     */
    void remove(Long userId);
}
