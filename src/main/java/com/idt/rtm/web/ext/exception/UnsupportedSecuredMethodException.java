package com.idt.rtm.web.ext.exception;

public class UnsupportedSecuredMethodException extends RuntimeException {

    public UnsupportedSecuredMethodException() {
        super("Only controller methods are available for authentication");
    }
}
