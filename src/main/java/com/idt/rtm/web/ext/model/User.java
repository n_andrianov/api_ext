package com.idt.rtm.web.ext.model;

import lombok.Data;

@Data
public class User {
    private Long id;
    private String login;
    private String mail;
    private String password;
    private String wallet;
}