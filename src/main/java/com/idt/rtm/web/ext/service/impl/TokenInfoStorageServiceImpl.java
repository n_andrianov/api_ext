package com.idt.rtm.web.ext.service.impl;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.policy.RecordExistsAction;
import com.aerospike.client.policy.WritePolicy;
import com.idt.rtm.web.ext.model.TokenStateEnum;
import com.idt.rtm.web.ext.model.UserTokenInfo;
import com.idt.rtm.web.ext.service.UserTokenInfoStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenInfoStorageServiceImpl implements UserTokenInfoStorageService {

    private final AerospikeClient aerospikeClient;
    private final int ttl;
    private final String aerospikeNamespace;
    private final String tokenSet;

    private final static String EXPIRES_BIN = "expires";
    private final static String STATE_BIN = "state";
    private final static String VALID_AFTER_BIN = "validAfter";
    private final static String USER_ID_BIN = "user_id";

    public TokenInfoStorageServiceImpl(AerospikeClient aerospikeClient,
                                       @Value("${aerospike.memory_namespace}") String aerospikeNamespace,
                                       @Value("${aerospike.set.tokens}") String tokenSet,
                                       @Value("${app.token.lifetime}") int ttl) {
        this.aerospikeClient = aerospikeClient;
        this.aerospikeNamespace = aerospikeNamespace;
        this.tokenSet = tokenSet;
        this.ttl = ttl;
    }

    // put or replace
    @Override
    public void put(Long userId, UserTokenInfo info) {
        WritePolicy wp = new WritePolicy();
        wp.recordExistsAction = RecordExistsAction.REPLACE;
        wp.expiration = ttl;
        Key key = new Key(aerospikeNamespace, tokenSet, userId);
        Bin validAfter = new Bin(VALID_AFTER_BIN, info.getValidAfter().getTime());
        Bin state = new Bin(STATE_BIN, info.getState().toString());
        Bin expires = new Bin(EXPIRES_BIN, info.getExpires().getTime());
        Bin user = new Bin(USER_ID_BIN, userId);
        aerospikeClient.put(wp, key, validAfter, state, expires, user);
    }

    @Override
    public UserTokenInfo get(Long userId) {
        Key key = new Key(aerospikeNamespace, tokenSet, userId);
        Record record = aerospikeClient.get(null, key);
        if (record != null) {
            UserTokenInfo uti = new UserTokenInfo();
            uti.setExpires(new Date(record.getLong(EXPIRES_BIN)));
            uti.setValidAfter(new Date(record.getLong(VALID_AFTER_BIN)));
            uti.setState(TokenStateEnum.valueOf(record.getString(STATE_BIN)));
            return uti;
        }
        return null;
    }

    @Override
    public void remove(Long userId) {
        Key key = new Key(aerospikeNamespace, tokenSet, userId);
        Bin validAfter = Bin.asNull(VALID_AFTER_BIN);
        Bin state = Bin.asNull(STATE_BIN);
        Bin expires = Bin.asNull(EXPIRES_BIN);
        aerospikeClient.put(null, key, validAfter, state, expires);
    }
}