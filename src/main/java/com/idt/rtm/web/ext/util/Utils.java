package com.idt.rtm.web.ext.util;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Utils {

    private static final String HEADER_PREFIX = "Bearer ";

    public static String extractTokenFromHeader(String headerValue) {
        return headerValue.replaceAll(HEADER_PREFIX, "");
    }

    public static java.util.Date getNewDate(Long tokenLifetime) {
        return java.util.Date.from(Instant.now().plus(tokenLifetime, ChronoUnit.SECONDS));
    }

    public static java.util.Date getCurrentDate() {
        return java.util.Date.from(Instant.now().truncatedTo(ChronoUnit.SECONDS));
    }

    public static java.util.Date getExpDate(Long tokenLifetime) {
        return java.util.Date.from(Instant.now().minus(tokenLifetime, ChronoUnit.SECONDS));
    }
}
