package com.idt.rtm.web.ext.service.impl;

import com.idt.rtm.web.ext.exception.UserAlreadyExistsException;
import com.idt.rtm.web.ext.exception.UserNotFoundByIdAndPasswordException;
import com.idt.rtm.web.ext.mapper.PostgresMapper;
import com.idt.rtm.web.ext.model.*;
import com.idt.rtm.web.ext.service.AuthorizationService;
import com.idt.rtm.web.ext.service.JwtService;
import com.idt.rtm.web.ext.service.UserTokenInfoStorageService;
import io.jsonwebtoken.Claims;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    private final PostgresMapper postgresMapper;
    private final JwtService jwtService;
    private final UserTokenInfoStorageService tokenInfoStorage;
    private final AuthenticatedUserContext user;

    private final String passwordSalt;
    private final String schema;

    public AuthorizationServiceImpl(PostgresMapper postgresMapper,
                                    JwtService jwtService,
                                    UserTokenInfoStorageService tokenInfoStorage,
                                    AuthenticatedUserContext user,
                                    @Value("${app.security.salt}") String passwordSalt,
                                    @Value("${app.schema_name}") String schema) {
        this.postgresMapper = postgresMapper;
        this.jwtService = jwtService;
        this.tokenInfoStorage = tokenInfoStorage;
        this.user = user;

        this.passwordSalt = passwordSalt;
        this.schema = schema;
    }

    @Override
    public LoginDto register(AuthDataDto authDataDto) {
        List<User> existingUsers = postgresMapper.getByLoginOrEmail(authDataDto.getLogin(), authDataDto.getMail(), schema);
        if (existingUsers == null || existingUsers.isEmpty()) {
            String encodedPassword = calcPasswordHash(authDataDto.getPassword());
            authDataDto.setPassword(encodedPassword);
            postgresMapper.addNew(authDataDto, schema);
            return login(authDataDto);
        } else {
            throw new UserAlreadyExistsException(authDataDto.getLogin(), authDataDto.getMail());
        }
    }

    @Override
    public LoginDto login(AuthDataDto authDataDto) {
        User user = postgresMapper.getUserByUsernameAndPasswordHash(authDataDto.getLogin(),
                calcPasswordHash(authDataDto.getPassword()), schema);
        if (user == null) {
            throw new UserNotFoundByIdAndPasswordException(authDataDto.getLogin());
        }
        log.trace("User {} exists", user.getLogin());

        String token = jwtService.generate(user);

        return new LoginDto(token, user.getLogin(), user.getMail());
    }

    @Override
    public void logout(String token) {
        jwtService.logout(token);
    }

    @Override
    public String checkAccess(String token){
        jwtService.isValid(token);

        Claims rawClaims = jwtService.getClaims(token);
        JwtClaims claims = new JwtClaims(rawClaims);

        // BAN/UPDATE flag ignored, reserved for future purposes
        UserTokenInfo uti = tokenInfoStorage.get(claims.userId);
        if (uti != null &&
                uti.getExpires().after(Date.from(Instant.now()))) {
            throw new SecurityException("Token banned");
        }

        fillUserContext(user, claims);
        return jwtService.refresh(rawClaims, claims.userId);
    }

    private String calcPasswordHash(String password) {
        return DigestUtils.md5Hex(passwordSalt + password);
    }

    private class JwtClaims {
        @SneakyThrows
        JwtClaims(Claims claims) {
            this.userId = Long.valueOf((String) claims.get(TokenClaims.ID));
            this.login = (String) claims.get(TokenClaims.LOGIN);
        }

        final Long userId;
        final String login;
    }

    private void fillUserContext(AuthenticatedUserContext user, JwtClaims claims) {
        user.setId(claims.userId);
        user.setLogin(claims.login);
    }
}
