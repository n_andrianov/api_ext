package com.idt.rtm.web.ext.exception;

import java.util.Date;

public class TokenExpiredException extends RuntimeException {

    public TokenExpiredException(Date date) {
        super(String.format("Token expired [%s]", date.toString()));
    }


}
