package com.idt.rtm.web.ext.aspect;

import com.idt.rtm.web.ext.annotation.Authenticated;
import com.idt.rtm.web.ext.annotation.SecureId;
import com.idt.rtm.web.ext.exception.UnsupportedSecuredMethodException;
import com.idt.rtm.web.ext.model.AuthenticatedUserContext;
import com.idt.rtm.web.ext.service.AuthorizationService;
import com.idt.rtm.web.ext.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Slf4j
@Aspect
@Order(1)
@Component
public class AuthAspect {

    private final AuthorizationService authorizationService;

    private final HttpServletRequest request;

    private final HttpServletResponse response;

    private final AuthenticatedUserContext userContext;

    public AuthAspect(AuthorizationService authorizationService,
                      HttpServletRequest request,
                      @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") HttpServletResponse response,
                      AuthenticatedUserContext userContext) {
        this.authorizationService = authorizationService;
        this.request = request;
        this.response = response;
        this.userContext = userContext;
    }

    @Before("@annotation(com.idt.rtm.web.ext.annotation.Authenticated)")
    void auth(JoinPoint jp) {
        if (request == null || response == null) {
            throw new UnsupportedSecuredMethodException();
        }
        if (request.getMethod().equals(HttpMethod.OPTIONS.name())) {
            response.setStatus(HttpStatus.OK.value());
            return;
        }

        String header = request.getHeader(HttpHeaders.AUTHORIZATION);

        // get token from header or request parameter
        String token;
        if (header != null) {
            token = Utils.extractTokenFromHeader(header);
        } else {
            token = request.getParameter("token");
        }

        if (token == null) {
            throw new SecurityException("Bearer token not found");
        }

        MethodSignature signature = (MethodSignature) jp.getSignature();
        Method method = signature.getMethod();

        Authenticated authAnnotation = method.getAnnotation(Authenticated.class);
        String refreshedToken = authorizationService.checkAccess(token);

        if (!userContext.isSuper()) {
            Parameter[] parameters = method.getParameters();
            int i = 0;
            Long secureId = null;
            while (i < parameters.length) {
                if (parameters[i].isAnnotationPresent(SecureId.class)) {
                    secureId = (Long) jp.getArgs()[i];
                    break;
                }
                i++;
            }
            if (secureId != null && !userContext.getId().equals(secureId)) {
                String message = String.format("Trying to access user's [%d] resource, but user current user id is [%d]",
                        secureId,
                        userContext.getId());
                throw new SecurityException(message);
            }
        }

        response.setHeader(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", refreshedToken));
    }
}
