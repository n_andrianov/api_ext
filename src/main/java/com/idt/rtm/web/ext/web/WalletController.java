package com.idt.rtm.web.ext.web;

import com.idt.rtm.web.ext.annotation.Authenticated;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.service.WalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class WalletController {

    private final WalletService walletService;

    public WalletController(WalletService walletService){
        this.walletService = walletService;
    }

    @Authenticated
    @RequestMapping(value = "/add_wallet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addWallet(@RequestBody @Validated User user) {
        walletService.addWallet(user);
        return ResponseEntity.ok("success");
    }
}
