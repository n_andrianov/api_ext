package com.idt.rtm.web.ext.exception;

public class InvalidTokenParamException extends RuntimeException {

    public InvalidTokenParamException(String param) {
        super(String.format("Invalid JWS param [%s]", param));
    }
}