package com.idt.rtm.web.ext.web;

import com.idt.rtm.web.ext.annotation.Authenticated;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.service.StatisticsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class StatisticsController {

    private final StatisticsService statisticsService;

    public StatisticsController(StatisticsService statisticsService){
        this.statisticsService = statisticsService;
    }

    @Authenticated
    @RequestMapping(value = "/statistics", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getStatistics(@RequestBody @Validated User user) {
        return ResponseEntity.ok(statisticsService.getStatistics(user));
    }
}
