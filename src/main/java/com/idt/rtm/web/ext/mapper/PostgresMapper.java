package com.idt.rtm.web.ext.mapper;

import com.idt.rtm.web.ext.model.AuthDataDto;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.model.UserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PostgresMapper {
    List<User> getByLoginOrEmail(
            @Param("login") String login,
            @Param("mail") String mail,
            @Param("schemaName") String schemaName
    );

    void addNew(
            @Param("user") AuthDataDto user,
            @Param("schemaName") String schemaName
    );

    User getUserByUsernameAndPasswordHash(
            @Param("username") String username,
            @Param("hashedPassword") String hashedPassword,
            @Param("schemaName") String schemaName
    );

    UserInfo getUserInfo(
            @Param("user") User user,
            @Param("schemaName") String schemaName
    );

    void addWallet(
            @Param("user") User user,
            @Param("schemaName") String schemaName
    );

    List<String> getUserWallets(
            @Param("user") User user,
            @Param("schemaName") String schemaName
    );
}
