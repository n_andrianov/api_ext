package com.idt.rtm.web.ext.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserTokenInfo {

    private TokenStateEnum state;

    // update/ban all tokens which were created before this date
    private Date validAfter;

    private Date expires;
}
