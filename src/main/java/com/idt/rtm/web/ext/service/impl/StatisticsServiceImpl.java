package com.idt.rtm.web.ext.service.impl;

import com.idt.rtm.web.ext.mapper.PostgresMapper;
import com.idt.rtm.web.ext.model.User;
import com.idt.rtm.web.ext.service.StatisticsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    private final PostgresMapper postgresMapper;

    private final String schema;

    public StatisticsServiceImpl(PostgresMapper postgresMapper,
                           @Value("${app.schema_name}") String schema){
        this.postgresMapper = postgresMapper;
        this.schema = schema;
    }

    @Override
    public String getStatistics(User user){
        return "mocked";//dummy
    }
}
